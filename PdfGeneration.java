
package fonebook;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import static java.awt.SystemColor.text;
import java.io.FileOutputStream;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;


public class PdfGeneration {
    public void pdfGeneration(String fileName, ObservableList<Person> data) {
        Document document = new Document();

        try {
            PdfWriter.getInstance(document, new FileOutputStream(fileName + ".pdf"));
            document.open();
            
            float[] columnWidths = {2, 4, 4, 6};
            PdfPTable table = new PdfPTable(columnWidths);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Phrase("KontaktLista"));
            cell.setBackgroundColor(GrayColor.CYAN);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);
            
            table.getDefaultCell().setBackgroundColor(GrayColor.MAGENTA);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell("Sorszám");
            table.addCell("Vezetéknév");
            table.addCell("Keresztnév");
            table.addCell("E-mail cím");
            table.setHeaderRows(1);
            
            table.getDefaultCell().setBackgroundColor(GrayColor.ORANGE);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            
            for (int i = 1; i <= data.size(); i++) {
                Person actualPerson = data.get(i - 1);
                
                table.addCell(""+i);
                table.addCell(actualPerson.getLastName());
                table.addCell(actualPerson.getFirstName());
                table.addCell(actualPerson.getEmail());
            }
            
            document.add(table);
            Chunk signature = new Chunk("\n\n Készült a FoneBook alkalmazással.");
            Paragraph base = new Paragraph(signature);
            document.add(base);
            
            document.close();
        }
    
    
     catch (Exception e) {
     Alert alert = new Alert(Alert.AlertType.INFORMATION);
     alert.setTitle("Valami nem jó!");
     alert.setHeaderText(null);
     alert.setContentText("A pdf-be való exportálás során valami hiba lépett fel!");
     alert.showAndWait();  
        
        
        }
}
}